using System.Collections.Generic;
using System.Linq;
using ActionFilters.Application.Exceptions;
using ActionFilters.Domain.Entities;

namespace ActionFilters.Application.Features
{
    public interface IBookService
    {
        Book GetById(int id);
    }
    
    public class BookService : IBookService
    {
        private IEnumerable<Book> _books = new List<Book>
        {
            new Book()
            {
                Id = 1,
                Name = "Book 1",
                RetailPrice = 19.99M
            },
            new Book()
            {
                Id = 2,
                Name = "Book 2",
                RetailPrice = 29.99M
            },
        };

public Book GetById(int id)
{
    var book = _books.FirstOrDefault(x => x.Id == id);

    if (book == null)
    {
        throw new EntityNotFoundException(nameof(Book), id);
    }

    return book;
}
    }
}